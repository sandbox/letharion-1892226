api = 2

core = 7.x

libraries[lite_accordion][download][type] = git
libraries[lite_accordion][download][url] = https://github.com/nikki/liteAccordion.git
libraries[lite_accordion][download][commit] = 372968366e
libraries[lite_accordion][directory_name] = "lite_accordion"
libraries[lite_accordion][destination] = "libraries"
